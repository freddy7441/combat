enchant();

window.onload = function() {
    var core = new Core(640, 400);
    core.preload(
	'chara1.png',
	'club.png',
	'icon1.png',
	'waku.png',
	'life.png',
	'map0.png'
    );
    core.fps = 15;
    core.onload = function() {


	var gameScene = new Scene();
	core.replaceScene(gameScene);
	
	// 背景
	var BackGround = Class.create(Sprite, {
	    initialize: function(x, y) {
		Sprite.call(this, 480, 304);
		this.backgroundColor = "rgb(0, 200, 255)";
		this.x = x;
		this.y = y;
		var maptip = core.assets['map0.png'];
		var image = new Surface(320, 320);
		for (var i = 0; i < 320; i += 16) {
		    image.draw(maptip, 0 * 16, 0, 16, 16, i, 304 - 16, 16, 16);
		    this.image = image;
		}		
		gameScene.addChild(this);
	    }
	});
	var b = new BackGround(80, 0);

	
	var User = Class.create(Sprite, {
	    initialize: function(x, y) {
		Sprite.call(this, 32, 32);
		this.x = x;
		this.y = y;
		this.image = core.assets['chara1.png'];
		this.on('enterframe', function() {
		    if (core.input.left) {
			if (uclub1.time <= 21) {
			    this.x -= CSPEED;
			    this.frame = this.age % 3;
			} else if (uclub1.time < 500 ) {
			    this.x -= TSPEED;
			    this.frame = this.age % 3;
			} else {}
		    }
		    if (core.input.right) {
			if (uclub1.time <= 21) {
			    this.x += CSPEED;
			    this.frame = this.age % 3;
			} else if (uclub1.time < 500 ) {
			    this.x += TSPEED;
			    this.frame = this.age % 3;
			} else {}
		    }

//		    if (core.input.up) this.y -= 5;
//		    if (core.input.down) this.y += 5;
		    
		    if (this.within(enemy, 70)) {
			if (uclub1.time <= 21 && eclub1.time > 21) {
			    //label.text = '右の勝ち';
			    uclub1.time = 500;
			    uclub2.time = 500;
			    uclub3.time = 500;
			    this.rotation = -90;
			    this.y = POSY + 10;
			}
			if (uclub1.time > 21 && eclub1.time <= 21) {
			    //label.text = '左の勝ち';
			    eclub1.time = 500;
			    eclub2.time = 500;
			    eclub3.time = 500;
			    enemy.rotation = 90;
			    enemy.y = POSY + 10;
			}

//			core.pushScene(gameOverScene);
//			core.stop();
		    } else {
//			label.text = 'ok';
		    }
		});
		gameScene.addChild(this);
	    },
	    restart: function(x, y) {
		this.x = x;
		this.y = y;
		this.rotation = 0;
	    }
	});

	var Enemy = Class.create(Sprite, {
	    initialize: function(x, y) {
		Sprite.call(this, 32, 32);
		this.x = x;
		this.y = y;
		this.image = core.assets['chara1.png'];
		this.frame = 5;
		this.scaleX *= -1;

		this.on('enterframe', function() {		    
		    if (core.input.up) {
			if (eclub1.time <= 21) {
			    this.x -= CSPEED;
			    this.frame = this.age % 3 + 5;
			} else if (eclub1.time < 500 ) {
			    this.x -= TSPEED;
			    this.frame = this.age % 3 + 5;
			} else {}
		    }
		    if (core.input.down) {
			if (eclub1.time <= 21) {
			    this.x += CSPEED;
			    this.frame = this.age % 3 + 5;
			} else if (eclub1.time < 500 ) {
			    this.x += TSPEED;
			    this.frame = this.age % 3 + 5;
			} else {}
		    }
//		    if (500 <= club1.time) this.rotation = 90;
		});
		gameScene.addChild(this);
	    },

	    restart: function(x, y) {
		this.x = x;
		this.y = y;
		this.rotation = 0;
	    }
	});

	var CSPEED = 3;
	var TSPEED = 5;
	
	var Club1 = Class.create(Sprite, {
	    initialize: function(x, y, data, opponentData) {
		Sprite.call(this, 32, 32);
		this.data = data;
		this.opponent = opponentData;
		this.sign = data.leftToRight ? 1 : -1;
		this.x = x;
		this.y = y-11;
		this.scaleX *= 0.8;
		this.scaleY *= 0.8;
		this.rotation = -130 * this.sign;
		this.image = core.assets['club.png'];
		this.time = 0;
		this.on('enterframe', function() {
		    if (this.time == 21) this.time = 0; else this.time += 1;
		    if (this.time <= 2) {
			this.y -= 8;
			this.rotate(-24 * this.sign);
		    } else if (this.time <= 5) {
			this.y -= 5;
			this.rotate(-36 * this.sign);
		    } else if (this.time <= 8) {
			this.y += 5;
			this.rotate(-36 * this.sign);
		    } else if (this.time <= 11) {
			this.y += 8;
			this.rotate(-24 * this.sign);
		    }  else if (this.time <= 16) {
			this.y += 2;
			this.rotate(10 * this.sign);
		    }  else if (this.time <= 21){
			this.y -= 2;
			this.rotate(-10 * this.sign);
		    } else if (100 == this.time) {
			this.y = y - 15;
			this.rotation = -130 * this.sign;
		    } else if (101 <= this.time && this.time <= (101+this.data.time)) { 
			this.x += this.data.swipeX;
			this.y -= (this.data.swipeY / this.data.time) * ((101+this.data.time-this.time) / this.data.time);
			this.rotate(-36 * this.sign);
		    } else if ((102+this.data.time) <= this.time && this.time <= (102+2*this.data.time)) {
			this.x += this.data.swipeX;
			this.y += (this.data.swipeY / this.data.time) * ((this.time-102-this.data.time) / this.data.time);
			this.rotate(-36 * this.sign);
		    } else if ((103+2*this.data.time) == this.time) {
				if (this.within(this.data.club2, 30)) {
	//			    ubel.text = 'catch';
					if (500 <= this.opponent.club1.time) {
						this.opponent.life[this.opponent.lifeNum].opacity = 0;
						this.opponent.lifeNum += 1;
						if (this.opponent.lifeNum == 3) {
							endText.setText(this.data.label + "の勝ち！");
							core.pushScene(gameOverScene);
							core.stop();
						} else {
							endReason.setText(this.data.label + "が生き残った");
							core.pushScene(preEndScene);			  
						}				
					} else {
						this.x = this.data.club2.x; // 移動中だと1本ずれる
						this.rotation = -130 * this.sign;
						this.time = 0;
					}
				} else {
	//			    ubel.text = 'drop';
					if (this.opponent.club1.time <= 21) {
						this.data.life[this.data.lifeNum].opacity = 0;
						this.data.lifeNum += 1;
						if (this.data.lifeNum == 3) {
							endText.setText(this.opponent.label + "の勝ち！");
							core.pushScene(gameOverScene);
							core.stop();
						} else {
							endReason.setText(this.data.label + "が落とした");
							core.pushScene(preEndScene);			   
						}
					} else if (this.opponent.club1.time < 500) {
						this.data.club1.time = 500;
						this.data.club2.time = 500;
						this.data.club3.time = 500;
						this.data.character.rotation = -90 * this.sign;
					} else {
						endReason.setText("両方落とした");				
						core.pushScene(preEndScene);			   				
					}
				}
		    } else if (500 <= this.time) {
				this.y = 270;
		    }
		    if (this.data.inputLeft() && this.time <= 21) this.x -= CSPEED;
		    if (this.data.inputRight() && this.time <= 21) this.x += CSPEED;
		});
		gameScene.addChild(this);		
	    },
	    
	    restart: function(x, y) {
		this.x = x;
		this.y = y - 11;
		this.rotation = -130 * this.sign;
		this.time = 0;		
	    }
	});

	
	var Club2 = Class.create(Sprite, {
	    initialize: function(x, y, data) {
		Sprite.call(this, 32, 32);
		this.data = data;
		this.sign = data.leftToRight ? 1 : -1;
		this.x = x;
		this.y = y-16;
		this.scaleX *= 0.8;
		this.scaleY *= 0.8;
		this.rotation = -58 * this.sign;
		this.image = core.assets['club.png'];
		this.time = 0;
		this.on('enterframe', function() {
		    if (this.time == 21) this.time = 0; else this.time += 1;
		    if (this.time <= 2) {
			this.y += 8;
			this.rotate(-24 * this.sign);
		    } else if (this.time <= 7) {
			this.y += 2;
			this.rotate(10 * this.sign);
		    } else if (this.time <= 12) {
			this.y -= 2;
			this.rotate(-10 * this.sign);
		    } else if (this.time <= 15) {
			this.y -= 8;
			this.rotate(-24 * this.sign);
		    } else if (this.time <= 18) {
			this.y -= 5;
			this.rotate(-36 * this.sign);
		    } else if (this.time <= 21){
			this.y += 5;
			this.rotate(-36 * this.sign);
		    } else if (100 <= this.time && this.time <= (102+2*this.data.time)) {
//			this.x -= 20;
			this.y = y;
			this.rotate(1000);
//			this.rotation = 180;
		    } else if ((103+2*this.data.time) == this.time) {
//			this.x += 20;
			this.y = y - 20;
			this.rotation = -58 * this.sign;
			this.time = 0;
		    } else if (500 <= this.time) {
			this.y = 270;
		    }


		    if (this.data.inputLeft()) {
			if (this.time <= 21) {
			    this.x -= CSPEED;
			} else if (this.time < 500 ) {
			    this.x -= TSPEED;
			} else {}
		    }
		    if (this.data.inputRight()) {
			if (this.time <= 21) {
			    this.x += CSPEED;
			} else if (this.time < 500 ) {
			    this.x += TSPEED;
			} else {}
		    }

		});
		gameScene.addChild(this);
	    },
	    restart: function(x, y) {
		this.x = x;
		this.y = y - 16;
		this.rotation = -58 * this.sign;
		this.time = 0;		
	    }
	});
	
	var Club3 = Class.create(Sprite, {
	    initialize: function(x, y, data) {
		Sprite.call(this, 32, 32);
		this.data = data;
		this.sign = data.leftToRight ? 1 : -1;
		this.x = x;
		this.y = y+10;
		this.scaleX *= 0.8;
		this.scaleY *= 0.8;
		this.rotation = -50 * this.sign;
		this.image = core.assets['club.png'];
		this.time = 0;	
		this.on('enterframe', function() {
		    if (this.time == 21) this.time = 0; else this.time += 1;
		    if (this.time == 0) {
			this.y += 2;
			this.rotate(10 * this.sign);
		    } else if (this.time <= 5) {
			this.y -= 2;
			this.rotate(-10 * this.sign);
		    } else if (this.time <= 8) {
			this.y -= 8;
			this.rotate(-24 * this.sign);
		    } else if (this.time <= 11) {
			this.y -= 5;
			this.rotate(-36 * this.sign);
		    } else if (this.time <= 14) {
			this.y += 5;
			this.rotate(-36 * this.sign);
		    } else if (this.time <= 17) {
			this.y += 8;
			this.rotate(-24 * this.sign);
		    } else if (this.time <= 21){
			this.y += 2;
			this.rotate(10 * this.sign);
		    } else if (100 <= this.time && this.time <= (102+2*this.data.time)) {
			this.y = y;
			this.rotate(-100);
//			this.rotation = -40;
		    } else if ((103+2*this.data.time) == this.time) {
			this.y = y + 8;
			this.rotation = -50 * this.sign;
			this.time = 0;
		    } else if (500 <= this.time) {
			this.y = 270;
		    }

			
		    if (this.data.inputLeft()) {
			if (this.time <= 21) {
			    this.x -= CSPEED;
			} else if (this.time < 500 ) {
			    this.x -= TSPEED;
			} else {}
		    }
		    if (this.data.inputRight()) {
			if (this.time <= 21) {
			    this.x += CSPEED;
			} else if (this.time < 500 ) {
			    this.x += TSPEED;
			} else {}
		    }
		});
		gameScene.addChild(this);
	    },

	    restart: function(x, y) {
		this.x = x;
		this.y = y + 10;
		this.rotation = -50 * this.sign;
		this.time = 0;		
	    }
	});

	var Yajirushi = Class.create(Sprite, {
	    initialize: function(x, y, r) {
		Sprite.call(this, 32, 32);
		this.x = x;
		this.y = y;
		this.scaleX *= 1.5;
		this.scaleY *= 1.5;
		this.rotation = r;
		this.image = core.assets['icon1.png'];
		gameScene.addChild(this);
		
	    }
	});

	var Pa = Class.create(Pad, {
	    initialize: function(x, y, r) {
		Pad.call(this);
		this.x = x;
		this.y = y;
		this.scaleX *= 1.2;
		this.scaleY *= 1.2;
//		this.opacity = 0;
		this.rotation = r;
		gameScene.addChild(this);
	    }
	});

	var Waku = Class.create(Sprite, {
	    initialize: function(x, y) {
		Sprite.call(this, 70, 200);
		this.x = x;
		this.y = y;
		this.image = core.assets['waku.png'];
		this.on('touchstart', function(e) {
		    if (uclub1.time <= 21 && 7 <= e.x && e.x <= 72 && 30 <= e.y && e.y <= 225) {
			uData.swipeX = e.x;
			uData.swipeY = e.y;
		    } else if (eclub1.time <= 21 && 567 <= e.x && e.x <= 633 && 30 <= e.y && e.y <= 225) {
			eData.swipeX = e.x;
			eData.swipeY = e.y;
		    }
		});
		this.on('touchend', function(e) {
		    if (uclub1.time <= 21 && 7 <= e.x && e.x <= 72 && 30 <= e.y && e.y <= 225) {
			uData.swipeX -= e.x;
			uData.swipeY -= e.y;

			uData.swipeX = Math.floor(uData.swipeX * -RATIOX);
			if (uData.swipeY < 0) uData.swipeY *= -RATIOY; else uData.swipeY *= RATIOY;
			uData.time = Math.floor(uData.swipeY / 15) + 2;
			
			uclub1.time = 99;
			uclub2.time = 99;
			uclub3.time = 99;
		    } else if (eclub1.time <= 21 && 567 <= e.x && e.x <= 633 && 30 <= e.y && e.y <= 225) {
			eData.swipeX -= e.x;
			eData.swipeY -= e.y;

			eData.swipeX = Math.floor(eData.swipeX * -RATIOX);
			if (eData.swipeY < 0) eData.swipeY *= -RATIOY; else eData.swipeY *= RATIOY; 
			eData.time = Math.floor(eData.swipeY / 15) + 2;
			
			eclub1.time = 99;
			eclub2.time = 99;
			eclub3.time = 99;
			
		    }
		    console.log(uData.swipeX);
		    console.log(uData.swipeY);
		    console.log(eData.swipeX);
		    console.log(eData.swipeY);
		    console.log(Math.floor(uData.swipeY / 10));
		});
		gameScene.addChild(this);
	    }
	});

	var Life = Class.create(Sprite, {
	    initialize: function(x, y) {
		Sprite.call(this, 32, 28);
		this.x = x;
		this.y = y;
		this.scaleX *= 0.5;
		this.scaleY *= 0.5;
		this.image = core.assets['life.png'];
		gameScene.addChild(this);
	    }
	});

	var uwaku = new Waku(5, 30);
	var ewaku = new Waku(565, 30);
	
	var POSY = 256;
	var UPOSX = 120;
	var EPOSX = 490;
	var SPEED = 1.2;
	var PADY = 310;
	var RATIOY = 3;
	var RATIOX = 0.1;
	    
	var user = new User(UPOSX-25, POSY);
	var enemy = new Enemy(EPOSX+25, POSY);

    var uData = {
		label: "左",
		swipeX: 0,
		swipeY: 0,
		time: 0,
		lifeNum: 0,
		life: [],
		leftToRight: true,
		character: user,
		inputLeft: function() {
			return core.input.left;
		},
		inputRight: function() {
			return core.input.right;
		},
	}
    var eData = {
		label: "右",
		swipeX: 0,
		swipeY: 0,
		time: 0,
		lifeNum: 0,
		life: [],
		leftToRight: false,
		character: enemy,
		inputLeft: function() {
			return core.input.up;
		},
		inputRight: function() {
			return core.input.down;
		},
	}

	for (var i = 0; i < 3; i++) {
	    uData.life[i] = new Life(130-(i*20), 20);
	}

	for (var i = 0; i < 3; i++) {
	    eData.life[i] = new Life(475+(i*20), 20);
	}

	var uclub1 = uData.club1 = new Club1(UPOSX, POSY, uData, /*opponent=*/eData);
	var uclub2 = uData.club2 = new Club2(UPOSX, POSY, uData);
	var uclub3 = uData.club3 = new Club3(UPOSX, POSY, uData);
	var eclub1 = eData.club1 = new Club1(EPOSX, POSY, eData, /*opponent=*/uData);
	var eclub2 = eData.club2 = new Club2(EPOSX, POSY, eData);
	var eclub3 = eData.club3 = new Club3(EPOSX, POSY, eData);




///* 指を離さないといけない．
	var uYaLeft = new Yajirushi(50, 310, 90);
	uYaLeft.buttonMode = 'left';
	var uYaRight = new Yajirushi(110, 310, -90);
	uYaRight.buttonMode = 'right';
	var uYaLeft = new Yajirushi(500, 310, 90);
	uYaLeft.buttonMode = 'up';
	var uYaRight = new Yajirushi(560, 310, -90);
	uYaRight.buttonMode = 'down';
//*/
	
/* 十字キーを隠すやつ．相手のキャラを操作できなくできればこれで良い．
	var uLeft = new Yajirushi(50, PADY, 90);
	var uRight = new Yajirushi(110, PADY, -90);
	var uPad = new Pa(45, 275, 0);

	var eLeft = new Yajirushi(500, PADY, 90);
	var eRight = new Yajirushi(560, PADY, -90);
	var ePad = new Pa(495, 275, 90);
*/
	
/* クマをドラッグして移動させようとした残骸．	
        var kuma = new User(33,22);
	kuma.on('touchstart', function (e) {
	    kuma.x = e.x-15;
	    kuma.y = e.y-20;
	});
	kuma.on('touchmove', function (e) {
	    kuma.x = e.x-15;
	    kuma.y = e.y-20;	    
	});
	kuma.on('enterframe', function () {
	    if (this.within(uLeft, 30)) {
	    }
	});
*/
	

	var La = Class.create(Label, {
	    initialize: function(x, y, text) {
		Label.call(this);
		this.x = x;
		this.y = y;
		this.text = text;
		gameScene.addChild(this);	
	    },
	    setText: function(text) {
		this.text = text;
	    }
	});
	    
//	var label = new La(300, 340, 'ok');
//	var ubel = new La(250, 320, "catch");
//	var ebel = new La(350, 320, "catch");




	var startScene = new Scene();
	var title = new La(440, 130, 'start');
	title.scaleX *= 2;
	title.scaleY *= 2;
	title.color = '#00ff00';
	startScene.addChild(title);
	core.pushScene(startScene);
	title.on('touchstart', function(e) {
	    core.popScene(startScene);
	});
	
	var preEndScene = new Scene();
	var endReason = new La(270, 170, '');
	endReason.color = '#00ff00';
	preEndScene.addChild(endReason);
	
	endReason.on('touchstart', function(e) {
	    
	    uclub1.restart(UPOSX, POSY);
	    uclub2.restart(UPOSX, POSY);
	    uclub3.restart(UPOSX, POSY);
	    eclub1.restart(EPOSX, POSY);
	    eclub2.restart(EPOSX, POSY);
	    eclub3.restart(EPOSX, POSY);
	    user.restart(UPOSX-25, POSY);
	    enemy.restart(EPOSX+25, POSY);
	    
	    core.popScene(preEndScene);
	});
	


	var gameOverScene = new Scene();
	var endText = new La(270, 170, '');
	endText.color = '#00ff00';
	gameOverScene.addChild(endText);

	
/*	var bears = [];
	for (var i = 0; i < 100; i++) {
	    bears[i] = new Bear(rand(320), rand(320));
	}

	    this.x += 5;
	    this.frame = this.age % 3 + 10;
	    if (this.x > 320) this.x = 0;
	    this.rotate(4);

	label.text = 0;
	label.on('enterframe', function() {
	    label.text = (core.frame / core.fps).toFixed(2);
	});

	bear.on('touchstart', function () {
	    core.rootScene.removeChild(this);
	});

	core.rootScene.on('touchstart', function (e) {
	    bear.x = e.x;
	    bear.y = e.y;
	});
*/	
    }

    core.start();
};

function rand(n) {
    return Math.floor(Math.random() * (n+1));
}
